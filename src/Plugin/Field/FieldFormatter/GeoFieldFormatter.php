<?php

namespace Drupal\geofield\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'geo_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "geo_field_formatter",
 *   label = @Translation("Geofield Formatter"),
 *   field_types = {
 *     "geo_field"
 *   }
 * )
 */
class GeoFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'geofield_show_as' => 'link',
        'geofield_linktext' => 'Map',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $geofield_output = array(
      'link' => $this->t('Link'),
      'map' => $this->t('Map'),
    );
    $element['geofield_show_as'] = array(
      '#title' => $this->t('Show geofield as'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('geofield_show_as'),
      '#options' => $geofield_output,
      '#description' => $this->t('Show as link or as map?'),
    );
    $element['geofield_linktext'] = array(
      '#title' => $this->t('Linktext'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('geofield_linktext'),
      '#description' => $this->t('Set the Text of the Link, shown on the Page.'),
      '#required' => FALSE,
      '#states' => array(
        'visible' => array(
          ':input[name$="[settings_edit_form][settings][geofield_show_as]"]' => array('value' => 'link'),
        ),
      ),
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getSetting('geofield_show_as')) {
      $summary[] = $this->t('Show as: @type', array('@type' => $this->getSetting('geofield_show_as')));
    }
    if ($this->getSetting('geofield_show_as') == 'link') {
      $summary[] = $this->t('Linktext: @type', array('@type' => $this->getSetting('geofield_linktext')));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @Todo Check values from Database before we build the output
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [$item];
    }

    $lat = (string) $item->lat;
    $long = (string) $item->long;
    $linktext = $this->getSetting('geofield_linktext');

    if ($this->getSetting('geofield_show_as') == 'link') {
      $elements[$delta] = ['#markup' => '<a href="http://maps.google.com/?q=' . $lat . ',' . $long . '" target="_blank"> ' . $linktext .' </a>'];
    }
    if ($this->getSetting('geofield_show_as') == 'map') {
      $elements[$delta] = ['#markup' => \Drupal\Core\Render\Markup::create(
        '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2370.3881631791137!2d' . $long . '!3d' . $lat . '!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNTPCsDMzJzAzLjAiTiA5wrA1NicyMi45IkU!5e0!3m2!1sde!2sus!4v1487601810316" width="300" height="225" frameborder="0" style="border:0" allowfullscreen></iframe>
      ')];
    }

    return $elements;
  }

}
