<?php

namespace Drupal\geofield\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'geo_field' widget.
 *
 * @FieldWidget(
 *   id = "geo_field",
 *   label = @Translation("Geofield"),
 *   field_types = {
 *     "geo_field"
 *   }
 * )
 */
class GeoField extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'geofield_limit_range' => 'no',
      'geofield_lat_min' => '',
      'geofield_lat_max' => '',
      'geofield_long_min' => '',
      'geofield_long_max' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $geofield_limitation = [
      'yes' => $this->t('Limit'),
      'no' => $this->t('No limit'),
    ];
    $elements['geofield_limit_range'] = [
      '#title' => $this->t('Limit the Range'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('geofield_limit_range'),
      '#options' => $geofield_limitation,
      '#description' => $this->t('Set limitation?'),
    ];
    $elements['geofield_lat_min'] = [
      '#title' => $this->t('Latitude minimum'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('geofield_lat_min'),
      '#description' => $this->t('Set the minimum Range for Latitude.'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][geofield_limit_range]"]' => ['value' => 'yes'],
        ],
      ],
    ];
    $elements['geofield_lat_max'] = [
      '#title' => $this->t('Latitude maximum'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('geofield_lat_max'),
      '#description' => $this->t('Set the maximum Range for Latitude.'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][geofield_limit_range]"]' => ['value' => 'yes'],
        ],
      ],
    ];
    $elements['geofield_long_min'] = [
      '#title' => $this->t('Longitude minimum'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('geofield_long_min'),
      '#description' => $this->t('Set the minimum Range for Longitude.'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][geofield_limit_range]"]' => ['value' => 'yes'],
        ],
      ],
    ];
    $elements['geofield_long_max'] = [
      '#title' => $this->t('Longitude maximum'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('geofield_long_max'),
      '#description' => $this->t('Set the maximum Range for Longitude.'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][geofield_limit_range]"]' => ['value' => 'yes'],
        ],
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    if ($this->getSetting('geofield_limit_range')) {
      $summary[] = $this->t('Range limitation: @type', array('@type' => $this->getSetting('geofield_limit_range')));
    }



    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $element['lat'] = [
      '#type' => 'textfield',
      '#title' => 'Latitude',
      '#default_value' => isset($items[$delta]->lat) ? $items[$delta]->lat : NULL,
      '#size' => $this->getSetting('size'),
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#element_validate' => array(
        array($this, 'validate'),
      ),
    ];
    $element['long'] = [
      '#type' => 'textfield',
      '#title' => 'Longitude',
      '#default_value' => isset($items[$delta]->long) ? $items[$delta]->long : NULL,
      '#size' => $this->getSetting('size'),
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#element_validate' => array(
        array($this, 'validate'),
      ),
    ];

    return $element;
  }

  /**
   * Validate the fields.
   */
  public function validate($element, FormStateInterface $form_state) {
    $field = $element['#title'];
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }
    if (!preg_match('/\d+\.\d{1,2}/', strtolower($value))) {
      $form_state->setError($element, t("Field must be a Float"));
    }
    if (($this->getSetting('geofield_limit_range') == 'yes') && (strlen($value) !== 0)) {
      $fieldname = '';

      if ($field == 'Latitude') {
        $fieldname = 'lat';
      }
      if ($field == 'Longitude') {
        $fieldname = 'long';
      }
      if ((round($value, 5) < round($this->getSetting('geofield_'.$fieldname.'_min'), 5))
        || (round($value, 5) > round($this->getSetting('geofield_'.$fieldname.'_max'), 5))){
        $min = $this->getSetting('geofield_'.$fieldname.'_min');
        $max = $this->getSetting('geofield_'.$fieldname.'_max');
        $form_state->setError($element, t("Field out of range. 
        Allowed Values: between $min and $max"));
      }
    }
  }

}
